require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:product) { create(:product) }

  background do
    visit potepan_product_path(product.id)
  end
  
  scenario "visit show" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end
end