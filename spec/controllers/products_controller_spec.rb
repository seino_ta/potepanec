require 'rails_helper'
RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let!(:product) { create(:product) }

    before { get :show, params: { id: product.id } }

    it "returns http success" do
      expect(response).to have_http_status 200
    end

    it "renders app/views/potepan/products/show.html.erb" do
      expect(response).to render_template :show
    end

    it "contain appropriate product" do
      expect(assigns(:product)).to eq(product)
    end
  end
end